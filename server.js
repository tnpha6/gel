
var tasks = [];
var task = {
  name: "Create Prototype",
  member: ["Thanh", "Andy"],
  deadline: "2-NOV-2014"
};

tasks.push(task);

//============== Get Dependencies ============================
//Get modules.
var express = require('express');
var routes = require('./routes');
var configAuth = require('./config');
var http = require('http');
var path = require('path');
var fs = require('fs');
var AWS = require('aws-sdk');
var app = express();

//============== Express ============================
//configure express
app.set('port', process.env.PORT || 3000);
//app.set('views', __dirname + '/public');
//app.engine('html', engines.mustache);
//app.set('view engine', 'html');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser()); // I would love to use this to manage my budget
app.use(express.session({ secret: 'IWL7U77MMB',
                          cookie: {
                              maxAge: 600000, // 60 minutes
                              httpOnly: false
                        }}));
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.locals.theme = process.env.THEME; //Make the THEME environment variable available to the app.
//============== AWS ============================
//Read config values from a JSON file.
var config = fs.readFileSync('./app_config.json', 'utf8');
config = JSON.parse(config);



//============== Routing ============================
//GET home page.
app.get('/', function(req, res){
  res.sendfile('public/index.html');
});

app.get('/api', function(req, res){
    res.send({"message": "helloooo!"});
});

app.get('/tasks', function(req, res){
  res.send(JSON.stringify(tasks));
});


//============== Functions ==========================

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
